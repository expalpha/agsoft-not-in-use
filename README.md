# ALPHA-g software contains
* TPC reconstruction and Barrel Scintillator
* Online Monitor
* Simulations

# Basic Requirements:
* root, e.g., version 6.08/06 - https://root.cern.ch/content/release-60806
* rootana - https://midas.triumf.ca/MidasWiki/index.php/ROOTANA

## For Simulations:
* geant4, e.g., version 10.3.1 - http://geant4.web.cern.ch/geant4/support/download.shtml
* cry (Cosmic RaY generator) - https://nuclear.llnl.gov/simulation/
* garfield++ - http://garfieldpp.web.cern.ch/garfieldpp/

# Source Tree
```
 $PWD
 |_ ana: low level data structures
 |_ reco: high level data structures
 |_ utils: utily classes, e.g., geometry, signals processing, etc.
 |_ sim
 |  |_ geant4
 |  |_ garfieldpp
 |_ aux: everything that is not code (STR tables, electronics config, etc.)
 |_ online: on-shift software
 |_ bin: executables
 |_ display: TBD
 |_ configuration script (bash)
```